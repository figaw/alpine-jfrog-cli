# jFrog CLI in docker

Run the jFrog cli as a docker container, without root privileges, with aliases for ease of use.

# Build

```
docker build -t jfrog-cli .
```

# Running

```
docker run jfrog-cli
```

# Aliases

```
alias jfrog='docker run \
    -v "$HOME/.jfrog":/non-privileged/.jfrog \
    jfrog-cli'
```

Including environment variables as configuration

```
alias jfroge='docker run \
    -e JFROG_CLI_LOG_LEVEL \
    -e JFROG_CLI_OFFER_CONFIG  \
    -v "$JFROG_CLI_HOME":/non-privileged/.jfrog \
    jfrog-cli'
```

The `JFROG_CLI_HOME` environment variable isn't passed as an environment variable,
but rather just mounted as a volume into the `~/.jfrog` location.

