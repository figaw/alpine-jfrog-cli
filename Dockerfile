FROM alpine:3.7

# Install packages, chmod startup script and add a non-privileged user
RUN apk --no-cache add curl \
                && adduser -D -u 1000 non-privileged \
                && mkdir /git \
                && chown -R 1000:1000 /git

# Notice the backslash in \$latest, ref: https://github.com/jfrog/jfrog-cli-go/issues/96
RUN curl -Lo /usr/bin/jfrog https://api.bintray.com/content/jfrog/jfrog-cli-go/\$latest/jfrog-cli-linux-386/jfrog?bt_package=jfrog-cli-linux-386 \
    && chmod a+x /usr/bin/jfrog

USER non-privileged

ENTRYPOINT ["jfrog"]
